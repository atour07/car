package com.carwificontrol;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.carwificontrol.databinding.ActivityMainBinding;

import communication.DataFromControlAndroid;


public class MainActivity extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener {


    private VerticalSeekBar seekbar;
    DriveModel dm=null;
    float mInitialX, mInitialY, mEndX, mEndY;
    float initialAccel, initialSteering;
    boolean drag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);



        dm=new DriveModel();



        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setModel(dm);
        seekbar = (VerticalSeekBar)findViewById(R.id.seekBar);
        seekbar.setMax(100);
        seekbar.setProgress(50);
        seekbar.setOnSeekBarChangeListener(this);
        final RotaryKnobView jogView = (RotaryKnobView)findViewById(R.id.view);
        jogView.setKnobListener(new RotaryKnobView.RotaryKnobListener() {
            @Override
            public void onKnobChanged(int arg) {
                    dm.setSteering(jogView.getAngle());
                DataFromControlAndroid dfa = new DataFromControlAndroid();
                dfa.setAcceleratioRate((int)(Float.parseFloat( dm.getAccel())));
                dfa.setSteeringWheelRate((int)Float.parseFloat(dm.getSteering()));

                CommunicationManagerInstance.getInstance().setDfa(dfa);
            }
        });


        CommunicationManagerInstance.Prepare("192.168.188.110", 45001);

        CommunicationManagerInstance.getInstance().Start(dm);

    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        // TODO Auto-generated method stub
         dm.setAccel(-(seekbar.getProgress()-50)*2);





        DataFromControlAndroid dfa = new DataFromControlAndroid();

        dfa.setAcceleratioRate((int)(Float.parseFloat( dm.getAccel())));
        dfa.setSteeringWheelRate((int)Float.parseFloat(dm.getSteering()));

        CommunicationManagerInstance.getInstance().setDfa(dfa);
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
