package com.carwificontrol;

import communication.JSONClient;

/**
 * Created by Мади on 15.06.2016.
 */
public class CommunicationManagerInstance {

    private static JSONClient instance;

    private static String host;
    private static int port;

    public static void Prepare(String _host, int _port){
        host = _host;
        port = _port;
    }

    public static synchronized JSONClient getInstance(){
        if(instance == null){
            instance = new JSONClient(host, port);

        }
        return instance;
    }



}
