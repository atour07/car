package com.carwificontrol;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by Мади on 08.06.2016.
 */
public class DriveModel  extends BaseObservable   {


    private  String steering="0";
    private  String accel="0";
    private  String speed="0";

    @Bindable
    public String getRpm() {
        return rpm;
    }

    public void setRpm(float rpm) {

        this.rpm = Float.toString(rpm);
        RPM=rpm;
        notifyPropertyChanged(com.carwificontrol.BR.rpm);
    }

    @Bindable
    public String getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = Float.toString(speed);
        SPEED=speed;
        notifyPropertyChanged(com.carwificontrol.BR.speed);
    }

    private  String rpm="0";

    public float getWheelAnglepercent() {
        return WheelAnglepercent;
    }

    public float getDinamicControlPercent() {
        return DinamicControlPercent;
    }

    private float WheelAnglepercent=0;
    private float DinamicControlPercent=0;
    private float RPM=0;
    private float SPEED=0;

    @Bindable
    public String getSteering() {
        return  steering;
    }

    @Bindable
    public String getAccel() {
        return   accel;
    }

    public void setAccel(float dinamicControlPercent) {

        accel =Float.toString( dinamicControlPercent);
        DinamicControlPercent = dinamicControlPercent;
        notifyPropertyChanged(com.carwificontrol.BR.accel);
    }

    public void setSteering(float wheelAnglepercent) {

        steering =Float.toString( wheelAnglepercent);
        WheelAnglepercent = wheelAnglepercent;
        notifyPropertyChanged(com.carwificontrol.BR.steering);
    }
}
