package communication;


import com.carwificontrol.DriveModel;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadFactory;

/**
 * Created by Мади on 15.06.2016.
 */
public class JSONClient
{// """
    //A JSON socket server used to communicate with a JSON socket client. All the
    //data is serialized in JSON. How to use it:

    String host;
    int port;
    Socket socket = null;
    List<ClientThread> ClientrThreads;
    DataFromCar dfc;
    DataFromControlAndroid dfa;

    public DataFromCar getLastDfc() {
        return dfc;
    }

    public void setLastDfc(DataFromCar dfc) {
        this.dfc = dfc;
    }



    public DataFromControlAndroid getDfa() {
        return dfa;
    }

    public void setDfa(DataFromControlAndroid dfa) {
        this.dfa = dfa;
    }

    public JSONClient(String host, int port)
    {
        this.host = host;
        this.port = port;
        dfc = new DataFromCar();
        dfa = new DataFromControlAndroid();
        ClientrThreads = new ArrayList<ClientThread>();
    }
   DriveModel dm;
    public void Start(DriveModel dm)
    {   this.dm=dm;
        try
        {
            ClientThread st;
            st = new ClientThread( this.host, this.port );
            Thread t = new Thread(st);
            t.start();
            ClientrThreads.add(st );
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        } // вывод исключений

    }

    private class ClientThread implements Runnable{

        Socket s;
        void Send(OutputStream outStream, DataFromControlAndroid androidData)
        {
            String toCarJSON = JSONTransferUtil.AndroidDataToString(androidData);
            SocketHelperCommunicator._send(outStream, toCarJSON.getBytes());
        }

        DataFromCar Receive(BufferedReader inpStream)
        {
            String fromCar = SocketHelperCommunicator._recv( inpStream);
            return JSONTransferUtil.StringToCarData(fromCar);
        }

        boolean NeedTerminate;

        public void setNeedTerminate(boolean NeedTerminate) {
            this.NeedTerminate = NeedTerminate;
        }

        public boolean isNeedTerminate() {
            return NeedTerminate;
        }
        public ClientThread(String host, int port)
        {
            // и запускаем новый вычислительный поток (см. ф-ю run())
        }
        @Override
        public void run()
        {
            try{
                this.s = new Socket(host, port);
                s.setTcpNoDelay(true);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            try
            {
                InputStream is = s.getInputStream();

                BufferedInputStream br = new BufferedInputStream(is);
                BufferedReader brr = new BufferedReader( new InputStreamReader(br));
                OutputStream os = s.getOutputStream();
                while(!isNeedTerminate()){

                    // из сокета клиента берём поток входящих данных

                    // и оттуда же - поток данных от сервера к клиенту

                    setLastDfc(Receive(brr));
                    dm.setRpm(getLastDfc().getEngineRPM());
                    dm.setSpeed(getLastDfc().getSpeed());

                    Send(os, getDfa());
                }
                s.close();

                System.out.println("Close");

            }
            catch(Exception e)
            {
                System.out.println("communicate error: "+e);} // вывод исключений
            }
    }
}
