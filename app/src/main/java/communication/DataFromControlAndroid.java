package communication;

/**
 * Created by Мади on 15.06.2016.
 */
public class DataFromControlAndroid   {
    private int acceleratioRate;
    private int steeringWheelRate;

    public void setAcceleratioRate(int acceleratioRate) {
        this.acceleratioRate = acceleratioRate;
    }

    public void setSteeringWheelRate(int steeringWheelRate) {
        this.steeringWheelRate = steeringWheelRate;
    }

    public int getAcceleratioRate() {
        return acceleratioRate;
    }

    public int getSteeringWheelRate() {
        return steeringWheelRate;
    }

}
