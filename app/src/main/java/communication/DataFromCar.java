package communication;

/**
 * Created by Мади on 15.06.2016.
 */
public class DataFromCar   {
    int speed;

    public DataFromCar() {

    }
    int engineRPM;

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setEngineRPM(int engineRPM) {
        this.engineRPM = engineRPM;
    }

    public int getSpeed() {
        return speed;
    }

    public int getEngineRPM() {
        return engineRPM;
    }
}
